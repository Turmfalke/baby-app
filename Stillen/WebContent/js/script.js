App = function(){
	
	var that = this;
	
	constructor();
	function constructor(){
		$("section").not(".start").hide(0);
		
		$("button[to]").click(function(e){
			var to = $(e.target).attr("to");
			
			if(to === undefined){
				to = $(e.target).parent("button").attr("to");
			}
			
			goTo(to);
		});
		
		$("button.store").click(function(e){
			addEntry($(e.target).parent("section").attr("class"));
		});
		
		$(".checkable").click(function(e){
			$(e.target).toggleClass("checked");
		});
		
		$("#stop").click(function(e){
			var now = getNow();
			
			console.log(now + " update");
			
			$("#stillzeitende")[0].value = now;
		});
	}
	
	function initStorage(){
		if(localStorage.getItem("com.chrisimnetz.tagebuch") === null){
			
			localStorage.setItem("com.chrisimnetz.tagebuch", JSON.stringify({ entries : [] }));
		}
	}
	
	function getNow(){
		var nowDate = new Date();
		nowDate.setTime( nowDate.getTime() + 120*60*1000 );
		
		return nowDate.toISOString().replace('Z', '');
	}
	
	function goTo(to){
		console.log("gehe zu: " + to);
		
		var toSection = $("section."+to);
		
		/* Handle Time Field Updates on Wickeln and Stillen */
		var timeFields = toSection.children(".zeit");
		
		if(timeFields !== undefined && timeFields.length > 0){
			var now = getNow();
			console.log(now);
			
			for(var i = 0; i < timeFields.length; i++){
				timeFields[i].value = now;
			}
		}
		
		$("section").not("."+to).hide(0, function(e){
			toSection.show(0);
			
			/* Put content from local storage on page */
			if(to === "tagebuch"){
				$("#tagebuchtext")[0].value = getDiaryText();
			}
		});
	}
	
	function addEntry(context){
		initStorage();
		
		var diary = JSON.parse(localStorage.getItem("com.chrisimnetz.tagebuch"));
		
		if(context === "wickeln"){
			diary.entries.push({ context : "gewickelt", time : $("#wickelzeit")[0].value, no1 : $("#urin").hasClass("checked"), no2 : $("#kot").hasClass("checked") });
			localStorage.setItem("com.chrisimnetz.tagebuch", JSON.stringify(diary));
			
			$(".checkable").removeClass("checked")
			
			goTo("start");
		}
		else if(context === "stillen"){
			diary.entries.push({ context : "gestillt", time1 : $("#stillzeit")[0].value, time2 : $("#stillzeitende")[0].value });
			localStorage.setItem("com.chrisimnetz.tagebuch", JSON.stringify(diary));
			goTo("start");
		}
	}
	
	function getDiaryText(){
		initStorage();
		
		var diary = JSON.parse(localStorage.getItem("com.chrisimnetz.tagebuch"));
		
		var output = "";
		for(var i = diary.entries.length-1; i >= 0; i--){
			var entry = diary.entries[i];
			
			console.log(entry);
			
			if(entry.context === "gewickelt"){
				content = entry.no1 === true && entry.no2 === true ? "beides" : entry.no1 === true ? "Urin" : entry.no2 === true ? "Kot" : "n/a";
				output += getDateString(entry.time) + ": " + entry.context + " (" + content + ")\n";
				
			}
			else if(entry.context === "gestillt"){
				output += getDateString(entry.time1) + ": " + entry.context + " bis " + getDateString(entry.time2, true) + "\n";
			}
		}
		
		$(".email").attr("href", "mailto:?body="+encodeURIComponent(output)+"&subject=Baby-App%20:%20Tagebuch");
		
		return output;
	}
	
	function getDateString(datetime, isTimeOnly){
		var split = datetime.split("T");
		var date = split[0];
		var time = split[1].split(".")[0]
		time = time.substr(0, time.length-3);
		return isTimeOnly === true ? time : date + " " + time;
	}
	
};


$(document).ready(function(){
	new App();
});