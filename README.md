# Baby-App #

Diese kleine Web-App habe ich für meine Frau geschrieben, damit sie Stillen und Windelwechsel auf einefache Art protokollieren kann. Das kann z.B. hilfreich sein um einen Stillrythmus zu prüfen.

Die Daten werden in den HTML5 local storage geschrieben und sind nur auf dem verwendenden Gerät verfügbar. Das Protokoll kann aber auch via e-mail verschickt werden.